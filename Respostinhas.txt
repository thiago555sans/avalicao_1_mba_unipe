3- Quais cuidados devem ser observados ao capturar dados de um site?
Hoje em dia a informação é um dos principais (senão o principal) ativos, por isso hoje iniciativas voltadas para aplicação de técnicas de governança de dados estão tendo bastante destaque entre as empresas, impulsionadas e inspiradas tanto pela GDPR quanto pela LGPD.
Do meu ponto de vista, pessoal, a partir do momento que a uma organização expõe uma determinada informação de maneira pública, com poucas ou até mesma nenhuma forma de proteção a essas informações, ela está assumindo o risco das inúmeras formas  que estes dados podem ser utilizados.
Acredito que o principal problema com práticas como web scraping seja de fato a finalidade do uso daquela informação, a prática em si é algo totalmente aceitável desde que não seja utilizado para fins ilícitos, como por exemplo, fraudes ou repassando informações de produtos e serviços de uma empresa para a concorrência.

4 - Quais ameaças capturas automáticas proporcionam para sistemas web?
O principal deles é o vazamento de informações consideradas estratégicas ou sensíveis para uma organização. Podendo também acontecer problemas como até mesmo indisponibilidade do sistema web devido a grande quantidade de requisições feitas ao servidor da aplicação.

5 - Você diria que bots ou crawlers são programas facilmente paralelizável? Se sim,
explique como isso seria implementado dando um exemplo
Do meu ponto de vista sim, são totalmente paralelizáveis. Por exemplo, podemos construir dois algoritmos que acessam simultaneamente a mesma página HTML, sendo que buscando informações diferentes, e isso poderia ser rodado facilmente através de ferramentas que trabalham com pipeline de dados como o apache airflow em uma mesma DAG e executando em paralelo. Talvez o problema que podemos enfrentar é que, alguns sites possuem mecanismos de segurança que podem por exemplo, ao vários acessos de um mesmo endereço IP a uma mesma página, essas requisições podem ser bloqueadas. 

Grupo: Thiago Santos - Francismar Júnior 
