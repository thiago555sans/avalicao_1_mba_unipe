from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import json

def finds(browser, by, expression):
    try:
        return browser.find_elements(by, expression)
    except NoSuchElementException as nse:
        return None

def find(browser, by, expression):
    try:
        return browser.find_element(by, expression)
    except NoSuchElementException as nse:
        return None

# Inserir o padrametro que se deseja buscar
produto = input('Qual produto deseja buscar?: \n')

# Criando o arquivo JSON
output  = open('output.json', 'w', encoding='utf-8')

# Definindo o Drive
driver = webdriver.Firefox()

#Definindo o endereço
driver.get("https://lista.mercadolivre.com.br/" + produto)

# Definindo items
items = finds(driver, By.CLASS_NAME, 'ui-search-result__content-wrapper')

# Capturar a descrição e o preço para cada item
for item in items:
    descricao = find(item, By.CLASS_NAME, 'ui-search-item__title').get_property("textContent")
    preco = find(item, By.CLASS_NAME,'price-tag-fraction').get_property("textContent")
    
    print(descricao)
    print(preco)
 
    item = {'descricao': descricao, 'preco': preco,}
  
    output.write(json.dumps(item))
    output.write('\n')
driver.close()
